#version 450
 
in vec3 frag_color;
 
out vec4 colorOut;
 
void main()
{
    colorOut = vec4(frag_color, 1.0);
}