#ifndef AQUILLA_UTILS_HASH_H_
#define AQUILLA_UTILS_HASH_H_

#include <aquilla/include.h>

namespace aquilla {

	UI64 hashString(const char *str);
}


#endif // !AQUILLA_UTILS_HASH_H_
