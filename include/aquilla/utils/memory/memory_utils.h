#ifndef AQUILLA_UTILS_MEMORY_MEMORY_UTILS_H_
#define AQUILLA_UTILS_MEMORY_MEMORY_UTILS_H_

#include <aquilla/include.h>

namespace aquilla {
namespace memory {

	constexpr size_t aligned_size(size_t size, size_t alignment) {
		return size + (size % alignment);
	}

} // namespace memory
} // namespace aquilla

#endif // !AQUILLA_UTILS_MEMORY_MEMORY_UTILS_H_
