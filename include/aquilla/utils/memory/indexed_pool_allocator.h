#ifndef AQUILLA_UTILS_MEMORY_INDEXED_POOL_ALLOCATOR_H_
#define AQUILLA_UTILS_MEMORY_INDEXED_POOL_ALLOCATOR_H_

#include <aquilla/include.h>
#include <aquilla/utils/memory/memory_utils.h>

#include <algorithm>
#include <limits>

namespace aquilla {

	template<typename T>
	class IndexedPoolAllocator {
	private:
		class BlockMetaInfo {
		public:
			UI32 next;
			UI32 prev;
		};

		union Block {
		public:
			BlockMetaInfo meta;
			T data;
		};

		static constexpr size_t kAlignment = 2;
		static constexpr size_t kBlockSize = memory::aligned_size(sizeof(Block), kAlignment); // TODO calculate alignment
		static constexpr UI32 kNoBlock = std::numeric_limits<UI32>::max();

	public:
		IndexedPoolAllocator();
		IndexedPoolAllocator(UI32 initial_size);
		~IndexedPoolAllocator();

		UI32 allocate();
		void deallocate(UI32);

		inline T* get(UI32);
		inline const T* get(UI32) const;

	private:
		UI32 size;

		Block *data;
		UI32 next_free_block;

		UI32 allocCount{ 0 };
		UI32 deallocCount{ 0 };

		void freeData();
		void resizeData(UI32 new_size);

		static UI32 initializeBlocks(Block* data, UI32 offset, UI32 count, UI32 prev);
	};


	//
	// Inline function definitions
	// 
	template<typename T>
	IndexedPoolAllocator<T>::IndexedPoolAllocator() : size{ 0 }, data{ nullptr }, next_free_block{ kNoBlock } {
	}

	template<typename T>
	IndexedPoolAllocator<T>::IndexedPoolAllocator(UI32 initial_size) : size{ initial_size } {
		if (size == 0) {
			data = nullptr;
			next_free_block = kNoBlock;
		}
		else {
			data = new Block[initial_size];
			UI32 last = initializeBlocks(data, 0, initial_size, kNoBlock);

			next_free_block = 0;
		}
	}

	template<typename T>
	IndexedPoolAllocator<T>::~IndexedPoolAllocator() {
		freeData();
	}

	template<typename T>
	UI32 IndexedPoolAllocator<T>::allocate() {
		allocCount++;

		if (next_free_block == kNoBlock) {
			UI32 new_size;
			if (size == 0) {
				new_size = 16;
			}
			else if (size < 1024) {
				new_size = size + 128;
			}
			else {
				new_size = size + 1024;
			}

			resizeData(new_size);
		}

		UI32 i = next_free_block;
		Block *block = &(data[i]);

		if (block->meta.next != kNoBlock) {
			data[block->meta.next].meta.prev = kNoBlock;
			next_free_block = block->meta.next;
		}
		else {
			next_free_block = kNoBlock;
		}

		return i;
	}

	template<typename T>
	void IndexedPoolAllocator<T>::deallocate(UI32 index) {
		deallocCount++;

		Block *block = &(data[index]);

		// The deallocated block is the only free block
		if (next_free_block == kNoBlock) {
			block->meta.next = kNoBlock;
			block->meta.prev = kNoBlock;

			next_free_block = index;

		// The deallocated block is the first free block
		} 
		else if (next_free_block > index) {
			block->meta.next = next_free_block;
			block->meta.prev = kNoBlock;

			data[next_free_block].meta.prev = index;
			next_free_block = index;
		} 
		else {
			UI32 prev_index = next_free_block;
			Block *prev = &(data[next_free_block]);

			// Find last free block before the deallocated block
			while (prev->meta.next < index) {
				prev_index = prev->meta.next;
				prev = &(data[prev_index]);
			}

			// The deallocated block is the last free block
			if (prev->meta.next == kNoBlock) {
				block->meta.next = kNoBlock;
				block->meta.prev = prev_index;

				prev->meta.next = index;
			} 
			else {
				block->meta.next = prev->meta.next;
				block->meta.prev = prev_index;

				prev->meta.next = index;
				data[block->meta.next].meta.prev = index;
			}
		}
	}

	template<typename T>
	inline T* IndexedPoolAllocator<T>::get(UI32 index) {
		return &(data[index].data);
	}

	template<typename T>
	inline const T* IndexedPoolAllocator<T>::get(UI32 index) const {
		return &(data[index].data);
	}

	template<typename T>
	void IndexedPoolAllocator<T>::freeData() {
		if (data) {
			delete[] data;
			data = nullptr;
		}

		size = 0;
		next_free_block = kNoBlock;
	}

	template<typename T>
	void IndexedPoolAllocator<T>::resizeData(UI32 new_size) {
		UI32 old_size = size;
		Block *old_data = data;

		UI32 last_block = next_free_block;
		if (last_block != kNoBlock) {
			while (data[last_block].meta.next != kNoBlock) {
				last_block = data[last_block].meta.next;
			}
		}

		data = new Block[new_size];
		size = new_size;
		if (old_data) {
			memcpy_s(data, size * sizeof(Block), old_data, old_size * sizeof(Block));
			delete[] old_data;

			initializeBlocks(data, old_size, new_size - old_size, last_block);
			if (last_block == kNoBlock) {
				next_free_block = old_size;
			}
		} 
		else {
			initializeBlocks(data, 0, size, kNoBlock);
			next_free_block = 0;
		}
	}

	template<typename T>
	UI32 IndexedPoolAllocator<T>::initializeBlocks(Block* data, UI32 offset, UI32 count, UI32 prev) {
		UI32 index = offset;
		while (index < (offset + count)) {
			data[index].meta.prev = prev;
			if (prev != kNoBlock) {
				data[prev].meta.next = index;
			}

			prev = index;
			index++;
		}
		data[index - 1].meta.next = kNoBlock;
		return index - 1;
	}

} // namespace aquilla

#endif // !AQUILLA_UTILS_MEMORY_INDEXED_POOL_ALLOCATOR_H_
