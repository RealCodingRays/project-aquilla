#ifndef AQUILLA_UTILS_OCTREE_H_
#define AQUILLA_UTILS_OCTREE_H_

#include <aquilla/include.h>
#include <aquilla/utils/memory/indexed_pool_allocator.h>

#include <type_traits>

namespace aquilla {
	 
	class Octree16 {
	public:
		static constexpr UI16 kHighBit = 1 << ((sizeof(UI16) * 8) - 1);

		class OctreeNode {
			UI16 children[8];
		};

		inline UI16 getRoot() const;
		inline OctreeNode* getNode(UI16 index);

	private:
		IndexedPoolAllocator<OctreeNode> nodes;

		UI16 root;
	};

} // namespace aquilla

#endif // !AQUILLA_UTILS_OCTREE_H_