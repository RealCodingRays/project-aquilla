#ifndef AQUILLA_WORLD_CHUNK_H_
#define AQUILLA_WORLD_CHUNK_H_

#include <aquilla/include.h>
#include <aquilla/utils/memory/indexed_pool_allocator.h>

#include <vector>
#include <glm/vec3.hpp>

namespace aquilla {

	class RaytraceHitInfo;

	class Mesh {
	public:
		using Lvec3 = glm::tvec3<I8>;
		void clear();
		void insertFace(Lvec3 p1, Lvec3 p2, Lvec3 p3, Lvec3 p4, Lvec3 normal);

		UI32 vertex_count{ 0 };
		std::vector<I8> vertex_data;
		std::vector<UI32> index_data;
	};

	class Chunk {
	public:
		using Lvec3 = glm::tvec3<I8>;
		static constexpr UI8 kSize = 32;

		void setVoxel(Lvec3 pos, bool obj);
		bool isFree(Lvec3 pos) const;

		void generateMesh() const;

		mutable Mesh mesh;

	private:
		/**
		 * Octree specialized for voxel storage inside chunks.
		 */
		class Octree {
		public: 
			/**
			 * The maximum number of layers inside the octreee.
			 *
			 * This is the maximum required size of an array storing the entire 
			 * stack of one datapoint including the root node and data.
			 */
			static constexpr UI8 kMaxLayers = 6;

			/**
			 * Bitmask set to indicate that the value is not a pointer to the next
			 * node but data.
			 */
			static constexpr UI16 kFlagMask = 0b1 << ((sizeof(UI16) * 8) - 1);

			/**
			 *
			 */
			static constexpr UI16 kMaxData = ~kFlagMask;

			/**
			 *
			 */
			static constexpr I8 kSizeForLayer[]{ 32, 16, 8, 4, 2, 1 };

			/**
			 * Datastructure defining one node inside the octree.
			 *
			 * The index of the next node can be calculated using the following formula:
			 *
 			 * index = 4 * x + 2 * y + z
			 * 
			 * Where x, y and z are 1 if the next node is in the positive direction of
			 * the axis and 0 if its in the negative direction.
			 */
			struct Node {
				/**
				 * Pointer to the next node or data.
				 */
				UI16 children[8];

				/**
				 * Converts a vector to the index of the child.
				 *
				 * See class description for more details.
				 *
				 * \param dir The direction of the next node.
				 * \return The index of the next node.
				 */
				static inline UI8 offsetToIndex(Lvec3 dir) {
					return dir.x * 4 + dir.y * 2 + dir.z;
				}

				/**
				 * Converts a vector to the index of the child.
				 *
				 * See class description for more details.
				 *
				 * \param x The direction on the x axis.
				 * \param y The direction on the y axis.
				 * \param z The direction on the z axis.
				 * \return The index of the next node.
				 */
				static inline UI8 offsetToIndex(I8 x, I8 y, I8 z) {
					return x * 4 + y * 2 + z;
				}

				/**
				 * Converts an index to a vector.
				 *
				 * See class description for more details.
				 *
				 * \param index The index of the next node.
				 * \return The direction of the next node.
				 */
				static inline Lvec3 indexToOffset(UI8 index) {
					return Lvec3{
						(index & 4) ? 1 : 0,
						(index & 2) ? 1 : 0,
						(index & 1) ? 1 : 0
					};
				}
			};

			/**
			 * 
			 */
			class TargetIterator {
			private:
				const Octree &tree;

				UI16 node_stack[kMaxLayers];
				UI8 max_layer{ 0 };

				Lvec3 target;

				void buildToLayer(UI8 layer);

			public:
				inline TargetIterator(const Octree &tree, Lvec3 target) : tree{ tree }, target{ target } {
					node_stack[0] = tree.root;
					if (isDataPtr(node_stack[0])) {
						for (UI8 i{ 1 }; i < kMaxLayers; i++) {
							node_stack[i] = tree.root;
						}
						max_layer = kMaxLayers - 1;
					}
				}

				inline UI16 operator[](UI8 layer) {
					buildToLayer(layer);
					return node_stack[layer];
				}

				inline UI16 max() {
					buildToLayer(kMaxLayers - 1);
					return node_stack[kMaxLayers - 1];
				}
			};

			/**
			 * Pointer to the root node or data if the entire chunk is uniform.
			 */
			UI16 root{ kFlagMask };

			/**
			 * All nodes.
			 */
			IndexedPoolAllocator<Node> nodes;

			/**
			 * Creates a new node and initializes all its children with one value.
			 *
			 * \param init The value that all of the nodes children should be set to.
			 * \return The pointer to the created node.
			 */
			UI16 createNode(UI16 init);

			/**
			 * Sets the data at a position.
			 *
			 * \param pos The position where the data will be stored.
			 * \param data The unflagged data to insert.
			 * \return Returns false if the position already contained that data. True otherwise.
			 */
			bool setData(Lvec3 pos, UI16 data);

			/**
			 * 
			 */
			UI16 getData(Lvec3 pos) const;

			static inline bool isDataPtr(UI16 ptr) {
				return ptr & kFlagMask;
			}

			static inline UI16 flaggedToData(UI16 ptr) {
				return ptr & (~kFlagMask);
			}
		};

		void insertFaceForNode(const Octree::Node* node, I8 size, Lvec3 offset, UI8 layer) const;

		Octree data;
	};
}

#endif // !AQUILLA_WORLD_CHUNK_H_
