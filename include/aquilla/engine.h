#ifndef AQUILLA_ENGINE_H_
#define AQUILLA_ENGINE_H_

#include <aquilla/include.h>
#include <aquilla/resource/group_manager.h>

namespace aquilla {

	struct Engine {
		GroupManager group_manager;
	};

	extern Engine engine;
}

#endif // !AQUILLA_ENGINE_H_
