#ifndef AQUILLA_RESOURCE_RESOURCE_MANAGER_H_
#define AQUILLA_RESOURCE_RESOURCE_MANAGER_H_

#include <aquilla/include.h>

#include <aquilla/resource/uuid.h>

#include <map>

namespace aquilla {

	class ResourceType {
	public:
		virtual ~ResourceType();

		virtual void* createResource(); // TODO <--------------------------------------------------
	};

	class ResourceManager {
	public:
		bool registerResourceType(UUID id, ResourceType* type);
		ResourceType* getResourceTypeClass(UUID type);



	private:
		struct ResourceEntry {
			
		};

		std::map<UUID, ResourceEntry> entries;
	};
}

#endif // !AQUILLA_RESOURCE_RESOURCE_MANAGER_H_
