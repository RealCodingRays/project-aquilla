#ifndef AQUILLA_RESOURCE_GROUP_MANAGER_H_
#define AQUILLA_RESOURCE_GROUP_MANAGER_H_

#include <aquilla/include.h>

#include <aquilla/resource/uuid.h>

#include <map>
#include <string>

namespace aquilla {

	class NameResolver {
	public:
		virtual ~NameResolver();

		virtual UI64 nameToID(const char* name) = 0;
		virtual const char* idToName(UI64 id) = 0;
	};

	class GroupManager {
	public:
		/**
		 * The number of ids reserved for static groups.
		 *
		 * All ids from 1 until and including this number are reserved.
		 */
		static constexpr UI16 kReservedGroups{ 100 };

		UI16 createGroup(const char* name, NameResolver* resolver = nullptr);
		bool createStaticGroup(UI16 group, const char* name, NameResolver* resolver);

		UI16 nameToGroup(const char* name);
		const char* groupToName(UI16 group);

		NameResolver* getNameResolver(UI16 group);

		UUID nameToUUID(const char* name);
		UUID nameToUUID(const char* group, const char* name);
		UUID nameToUUID(UI16 group, const char* name);

		// uuidToName(UUID uuid); TODO

	private:
		std::map<std::string, UI16> name_to_id;
		std::map<UI16, std::string> id_to_name;
		std::map<UI16, NameResolver*> name_resolvers;

		UI16 next_index{ kReservedGroups + 1 };
	};
}

#endif // !AQUILLA_RESOURCE_GROUP_MANAGER_H_
