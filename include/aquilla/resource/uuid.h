#ifndef AQUILLA_RESOURCE_UUID_H // TODO ---------------------------------------------------------
#define AQUILLA_RESOURCE_UUID_H_

#include <aquilla/include.h>

namespace aquilla {

	class UUID {
	public:
		static constexpr UI64 kInvalid{ 0 };
		static constexpr UI64 kGroupMask{ 0xFFFF000000000000 };
		static constexpr UI64 kItemMask{ ~kGroupMask };

	protected:
		UI64 uuid{ 0 };

		inline void _setGroup(UI16 group) {
			uuid &= kItemMask;
			uuid |= static_cast<UI64>(group) << 48;
		}

		inline void _setItem(UI64 data) {
			uuid &= kGroupMask;
			uuid |= (data & kItemMask);
		}

		inline UI16 _getGroup() const {
			return (uuid & kGroupMask) >> 48;
		}

		inline UI64 _getItem() const {
			return uuid & kItemMask;
		}

		inline void _invalidate() {
			uuid = kInvalid;
		}

		inline bool _isValid() const {
			return uuid != kInvalid;
		}

	public:
		inline UUID() {
		}

		inline UUID(const UUID &other) : uuid{ other.uuid } {
		}

		inline UUID(UUID &&other) : uuid{ other.uuid } {
		}

		inline UUID(UI16 group, UI64 item) : uuid{ (static_cast<UI64>(group) << 48) | (item & kItemMask) } {
		}

		UUID(const char *name);
		UUID(const char *group, const char *item);

		inline explicit UUID(UI64 uuid) : uuid{ uuid } {
		}

		inline UUID& operator =(const UUID &other) {
			uuid = other.uuid;
			return *this;
		}

		inline UUID& operator =(UUID &&other) {
			uuid = other.uuid;
			return *this;
		}

		inline UUID& operator =(UI64 other) {
			uuid = other;
			return *this;
		}

		UUID &operator =(const char *name);

		inline operator UI64() const {
			return uuid;
		}

		inline bool operator ==(UUID other) const {
			return other.uuid == uuid;
		}

		inline bool operator !=(UUID other) const {
			return other.uuid != uuid;
		}

		inline bool operator >(UUID other) const {
			return uuid > other.uuid;
		}

		inline bool operator >=(UUID other) const {
			return uuid >= other.uuid;
		}

		inline bool operator <(UUID other) const {
			return uuid < other.uuid;
		}

		inline bool operator <=(UUID other) const {
			return uuid <= other.uuid;
		}

		inline operator bool() const {
			return _isValid();
		}

		inline bool isValid() const {
			return _isValid();
		}

		inline UI16 getGroup() const {
			return _getGroup();
		}

		inline UI64 getItem() const {
			return _getItem();
		}

		inline void setGroup(UI16 group) {
			_setGroup(group);
		}

		void setGroup(const char *group);

		inline void setItem(UI64 item) {
			_setItem(item);
		}

		void setItem(const char *item);

		inline void set(UI16 group, UI64 item) {
			uuid = (static_cast<UI64>(group) << 48) | (item & kItemMask); // TODO move code to protected function
		}

		void set(const char *name);

		void set(const char *group, const char *item);
	};
}

#endif // !AQUILLA_RESOURCE_UUID_H_
