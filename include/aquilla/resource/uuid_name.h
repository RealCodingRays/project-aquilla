#ifndef AQUILLA_RESOURCE_UUID_NAME_H_
#define AQUILLA_RESOURCE_UUID_NAME_H_

#include <aquilla/include.h>

namespace aquilla {

	namespace uuid {

		constexpr char kSeparator{ ':' };

		inline bool isValidCharacter(char chr) {
			return (chr >= 'a' && chr <= 'z') || (chr >= 'A' && chr <= 'Z') || (chr >= '0' && chr <= '9') || (chr == '_');
		}

		bool isValidName(const char* name);
		bool isValidGroupName(const char* name);
		bool isValidItemName(const char* name);

		bool parseName(const char* name, size_t &group_length, size_t &item_length);

		void generateHexName(char* ptr, UI64 item);
	}
}

#endif // !AQUILLA_RESOURCE_UUID_NAME_H_
