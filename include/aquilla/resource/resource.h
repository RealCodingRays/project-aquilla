#ifndef AQUILLA_RESOURCE_RESOURCE_H_
#define AQUILLA_RESOURCE_RESOURCE_H_

#include <aquilla/include.h>

namespace aquilla {

	class ResourceInstance;

	template<typename R>
	class ResourceReference {
	private:
		template<typename O>
		friend class ResourceReference<O>;

		ResourceInstance* instance{ 0 };
		UI64* ref_counter{ 0 };

		inline void _release() {
			if (instance) {
				(*ref_counter)--;
			}
			instance = nullptr;
			ref_counter = nullptr;
		}

		inline void _aquire(ResourceInstance* inst, UI64* ref) {
			if (instance) {
				(*ref_counter)--;
			}

			if (inst && ref) {
				instance = inst;
				ref_counter = ref;
				(*ref_counter)++;
			}
			else {
				instance = nullptr;
				ref_counter = nullptr;
			}
		}

	public:
		inline ResourceReference() {
		}

		inline ResourceReference(ResourceInstance* inst, UI64* ref) {
			if (dynamic_cast<R>(inst)) {
				_aquire(inst, ref);
			}
		}

		template<typename O>
		inline ResourceReference(const ResourceReference<O> &other) {
			if (dynamic_cast<R>(other.instance)) {
				_aquire(other.instance, other.ref_counter);
			}
		}

		template<typename O>
		inline ResourceReference(ResourceReference<O> &&other) {
			_release();
			if (dynamic_cast<R>(other.instance)) {
				this->instance = other.instance;
				this->ref_counter = other.ref_counter;
				other.instance = nullptr;
				other.ref_counter = nullptr;
			}
		}

		inline ~ResourceReference() {
			_release();
		}
	};
}

#endif // !AQUILLA_RESOURCE_RESOURCE_H_
