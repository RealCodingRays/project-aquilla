#ifndef AQUILLA_RESOURCE_NAME_RESOLVER_BASIC_HASH_RESOLVER_H_
#define AQUILLA_RESOURCE_NAME_RESOLVER_BASIC_HASH_RESOLVER_H_

#include <aquilla/resource/group_manager.h>

namespace aquilla {

	class BasicHashResolver : public NameResolver {
	public:
		virtual UI64 nameToID(const char* name);
		virtual const char* idToName(UI64 id);
	};
}

#endif // !AQUILLA_RESOURCE_NAME_RESOLVER_BASIC_HASH_RESOLVER_H_
