#version 450

layout(location = 0) in ivec3 pos;
layout(location = 1) in ivec3 normal;

uniform mat4 mtow;
uniform mat4 wtos;
uniform vec3 view;

out vec3 frag_color;

void main() {
	gl_Position = wtos * mtow * vec4(pos, 1);

	frag_color = vec3(0, 1, 0) * ((dot(normal, view) + 1.) / 2.) + vec3(((dot(normal, vec3(1, 0, 0)) + 1.) / 2.), 0, 0);
}