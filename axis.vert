#version 450

layout(location = 0) in ivec3 pos;
layout(location = 1) in ivec3 color;

uniform mat4 mtow;
uniform mat4 wtos;

out vec3 frag_color;

void main() {
	gl_Position = wtos * mtow * vec4(pos, 1);

	frag_color = color;
}