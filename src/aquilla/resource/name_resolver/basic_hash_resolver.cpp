#include <aquilla/resource/name_resolver/basic_hash_resolver.h>

#include <xxhash/xxhash.hpp>

#include <cstring>

namespace aquilla {
	
	UI64 BasicHashResolver::nameToID(const char* name) {
		return xxh::xxhash<64>(name, std::strlen(name)) & UUID::kItemMask;
	}

	const char* BasicHashResolver::idToName(UI64 id) {
		char* ret = new char[1 + 12 + 1]; // '#' + 12 digits + null terminator
		ret[0] = '#';
		ret[13] = 0;
		return ret;
	}
}