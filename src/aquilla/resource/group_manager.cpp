#include <aquilla/resource/group_manager.h>

#include <aquilla/resource/uuid_name.h>
#include <aquilla/resource/name_resolver/basic_hash_resolver.h>

namespace aquilla {

	NameResolver::~NameResolver() {
	}

	BasicHashResolver default_resolver;

	UI16 GroupManager::createGroup(const char* name, NameResolver* resolver) {
		std::string str{ name };
		auto it = name_to_id.find(str);
		if (it != name_to_id.end()) {
			return 0;
		}

		UI16 id = next_index++;

		name_to_id[str] = id;
		id_to_name[id] = str;
		name_resolvers[id] = resolver ? resolver : &default_resolver;

		return id;
	}

	bool GroupManager::createStaticGroup(UI16 group, const char* name, NameResolver* resolver) {
		std::string str{ name };
		if (group == 0 || group > kReservedGroups) {
			return false;
		}

		auto its = name_to_id.find(str);
		if (its != name_to_id.end()) {
			return false;
		}

		auto iti = id_to_name.find(group);
		if (iti != id_to_name.end()) {
			return false;
		}

		name_to_id[str] = group;
		id_to_name[group] = str;
		name_resolvers[group] = resolver ? resolver : &default_resolver;

		return true;
	}

	UI16 GroupManager::nameToGroup(const char* name) {
		std::string str{ name };
		auto it = name_to_id.find(name);
		if (it == name_to_id.end()) {
			return 0;
		}

		return it->second;
	}

	const char* GroupManager::groupToName(UI16 group) {
		auto it = id_to_name.find(group);
		if (it == id_to_name.end()) {
			return nullptr;
		}

		return it->second.data();
	}

	NameResolver* GroupManager::getNameResolver(UI16 group) {
		auto it = name_resolvers.find(group);
		if (it == name_resolvers.end()) {
			return nullptr;
		}

		return it->second;
	}

	UUID GroupManager::nameToUUID(const char* name) {
		size_t group_length;
		size_t item_length;
		if (!uuid::parseName(name, group_length, item_length)) {
			return UUID{};
		}

		std::string str{ name, group_length };
		auto it = name_to_id.find(str);
		if (it == name_to_id.end()) {
			return UUID{};
		}
		UI16 group = it->second;

		NameResolver* resolver = getNameResolver(group);
		if (!resolver) {
			return UUID{};
		}

		return UUID{ group, resolver->nameToID(&(name[group_length + 1])) };
	}

	UUID GroupManager::nameToUUID(const char* group, const char* item) {
		if (!uuid::isValidGroupName(group)) {
			return UUID{};
		}
		if (!uuid::isValidItemName(item)) {
			return UUID{};
		}

		std::string str{ group };
		auto it = name_to_id.find(str);
		if (it == name_to_id.end()) {
			return UUID{};
		}
		UI16 id = it->second;

		NameResolver* resolver = getNameResolver(id);
		if (!resolver) {
			return UUID{};
		}

		return UUID{ id, resolver->nameToID(item) };
	}

	UUID GroupManager::nameToUUID(UI16 group, const char* item) {
		if (!uuid::isValidItemName(item)) {
			return UUID{};
		}

		NameResolver* resolver = getNameResolver(group);
		if (!resolver) {
			return UUID{};
		}

		return UUID{ group, resolver->nameToID(item) };
	}
}