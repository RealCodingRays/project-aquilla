#include <aquilla/resource/uuid_name.h>

#include <cstring>

namespace aquilla {
	namespace uuid {

		bool isValidName(const char* name) {
			if(!name) {
				return false;
			}

			size_t length{ std::strlen(name) };
			size_t sep_index{ 0 };
			for (size_t i{ 0 }; i < length; i++) {
				if (name[i] == kSeparator) {
					if (sep_index != 0) {
						return false; // Multiple instances of ':'
					}
					if (i == 0) {
						return false; // No group name
					}
					if (i == length - 1) {
						return false; // No item name
					}
					sep_index = i;
				}
				else {
					if (!isValidCharacter(name[i])) {
						return false; // Invalid character
					}
				}
			}

			return true;
		}

		bool isValidGroupName(const char *name) {
			if (!name) {
				return false;
			}

			size_t length{ std::strlen(name) };
			for (size_t i{ 0 }; i < length; i++) {
				if (!isValidCharacter(name[i])) {
					return false;
				}
			}

			return true;
		}

		bool isValidItemName(const char *name) {
			if (!name) {
				return false;
			}

			size_t length{ std::strlen(name) };
			for (size_t i{ 0 }; i < length; i++) {
				if (!isValidCharacter(name[i])) {
					return false;
				}
			}

			return true;
		}

		bool parseName(const char* name, size_t &group_length, size_t &item_length) {
			if (!name) {
				return false;
			}

			size_t length{ std::strlen(name) };
			size_t sep_index{ 0 };
			for (size_t i{ 0 }; i < length; i++) {
				if (name[i] == kSeparator) {
					if (sep_index != 0) {
						return false; // Multiple instances of ':'
					}
					if (i == 0) {
						return false; // No group name
					}
					if (i == length - 1) {
						return false; // No item name
					}
					sep_index = i;
					group_length = sep_index;
					item_length = length - sep_index;
				}
				else {
					if (!isValidCharacter(name[i])) {
						return false; // Invalid character
					}
				}
			}

			return true;
		}
	}
}