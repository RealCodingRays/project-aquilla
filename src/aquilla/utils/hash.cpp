#include <aquilla/utils/hash.h>

#include <xxhash/xxhash.hpp>

#include <cstring>

namespace aquilla {

	UI64 hashString(const char *str) {
		return xxh::xxhash<64>(str, strlen(str));
	}
}