#include <aquilla/world/chunk.h>

#include <iostream>

namespace aquilla {

	void Mesh::clear() {
		vertex_count = 0;
		vertex_data.clear();
		index_data.clear();
	}

	void Mesh::insertFace(Lvec3 p1, Lvec3 p2, Lvec3 p3, Lvec3 p4, Lvec3 normal) {
		vertex_data.push_back(p1.x);
		vertex_data.push_back(p1.y);
		vertex_data.push_back(p1.z);
		vertex_data.push_back(normal.x);
		vertex_data.push_back(normal.y);
		vertex_data.push_back(normal.z);

		vertex_data.push_back(p2.x);
		vertex_data.push_back(p2.y);
		vertex_data.push_back(p2.z);
		vertex_data.push_back(normal.x);
		vertex_data.push_back(normal.y);
		vertex_data.push_back(normal.z);

		vertex_data.push_back(p3.x);
		vertex_data.push_back(p3.y);
		vertex_data.push_back(p3.z);
		vertex_data.push_back(normal.x);
		vertex_data.push_back(normal.y);
		vertex_data.push_back(normal.z);

		vertex_data.push_back(p4.x);
		vertex_data.push_back(p4.y);
		vertex_data.push_back(p4.z);
		vertex_data.push_back(normal.x);
		vertex_data.push_back(normal.y);
		vertex_data.push_back(normal.z);

		if (vertex_count != 0) {
			index_data.push_back(0xFFFF);
		}
		index_data.push_back(vertex_count++);
		index_data.push_back(vertex_count++);
		index_data.push_back(vertex_count++);
		index_data.push_back(vertex_count++);
	}


	void Chunk::setVoxel(Chunk::Lvec3 pos, bool obj) {
		data.setData(pos, obj ? 1 : 0);
	}

	bool Chunk::isFree(Chunk::Lvec3 pos) const {
		return data.getData(pos) == 0;
	}

	void Chunk::generateMesh() const {
		mesh.clear();

		if (Octree::isDataPtr(data.root)) {
			UI16 val = Octree::flaggedToData(data.root);

			if (val != 0) {
				// Front face
				mesh.insertFace(
					{0, 0, 0},
					{0, kSize, 0},
					{kSize, kSize, 0},
					{kSize, 0, 0},
					{0, 0, -1}
				);

				// Right face
				mesh.insertFace(
					{kSize, 0, 0},
					{kSize, kSize, 0},
					{kSize, kSize, kSize},
					{kSize, 0, kSize},
					{1, 0, 0}
				);

				// Back face
				mesh.insertFace(
					{kSize, 0, kSize},
					{kSize, kSize, kSize},
					{0, kSize, kSize},
					{0, 0, kSize},
					{0, 0, 1}
				);

				// Left face
				mesh.insertFace(
					{0, 0, kSize},
					{0, kSize, kSize},
					{0, kSize, 0},
					{0, 0, 0},
					{-1, 0, 0}
				);

				// Top face
				mesh.insertFace(
					{0, kSize, 0},
					{0, kSize, kSize},
					{kSize, kSize, kSize},
					{kSize, kSize, 0},
					{0, 1, 0}
				);

				// Bottom face
				mesh.insertFace(
					{kSize, 0, 0},
					{kSize, 0, kSize},
					{0, 0, kSize},
					{0, 0, 0},
					{0, -1, 0}
				);
			}
		}
		else {
			insertFaceForNode(data.nodes.get(data.root), kSize / 2, Lvec3{ 0, 0, 0 }, 1);
		}
	}

	void Chunk::insertFaceForNode(const Octree::Node* node, I8 size, Lvec3 offset, UI8 layer) const {
		for (I8 x{ 0 }; x < 2; x++) {
			for (I8 y{ 0 }; y < 2; y++) {
				for (I8 z{ 0 }; z < 2; z++) {
					UI8 index = x * 4 + y * 2 + z;

					Lvec3 on{ offset.x + x * size, offset.y + y * size, offset.z + z * size };

					if (!Octree::isDataPtr(node->children[index])) {
						insertFaceForNode(data.nodes.get(node->children[index]), size / 2, on, layer + 1);
					}
					else {
						if (Octree::flaggedToData(node->children[index])) {
							UI16 adj;

							// Front face
								mesh.insertFace(
									{ on.x, on.y, on.z },
									{ on.x, on.y + size, on.z },
									{ on.x + size, on.y + size, on.z },
									{ on.x + size, on.y, on.z },
									{ 0, 0, -1 }
								);

							// Right face
								mesh.insertFace(
									{ on.x + size, on.y, on.z },
									{ on.x + size, on.y + size, on.z },
									{ on.x + size, on.y + size, on.z + size },
									{ on.x + size, on.y, on.z + size },
									{ 1, 0, 0 }
								);

							// Back face
								mesh.insertFace(
									{ on.x + size, on.y, on.z + size },
									{ on.x + size, on.y + size, on.z + size },
									{ on.x, on.y + size, on.z + size },
									{ on.x, on.y, on.z + size },
									{ 0, 0, 1 }
								);

							// Left face
								mesh.insertFace(
									{ on.x, on.y, on.z + size },
									{ on.x, on.y + size, on.z + size },
									{ on.x, on.y + size, on.z },
									{ on.x, on.y, on.z },
									{ -1, 0, 0 }
								);

							// Top face
								mesh.insertFace(
									{ on.x, on.y + size, on.z },
									{ on.x, on.y + size, on.z + size },
									{ on.x + size, on.y + size, on.z + size },
									{ on.x + size, on.y + size, on.z },
									{ 0, 1, 0 }
								);

							// Bottom face
								mesh.insertFace(
									{ on.x + size, on.y, on.z },
									{ on.x + size, on.y, on.z + size },
									{ on.x, on.y, on.z + size },
									{ on.x, on.y, on.z },
									{ 0, -1, 0 }
								);
						}
					}
				}
			}
		}
	}





	void Chunk::Octree::TargetIterator::buildToLayer(UI8 layer) {
		while (true) {
			UI8 current_layer = max_layer + 1;
			if (current_layer >= layer) {
				return;
			}

			I8 size = kSizeForLayer[current_layer];
			Lvec3 dir{ 0, 0, 0 };

			if (target.x - size >= 0) {
				target.x -= size;
				dir.x++;
			}
			if (target.y - size >= 0) {
				target.y -= size;
				dir.y++;
			}
			if (target.z - size >= 0) {
				target.z -= size;
				dir.z++;
			}

			UI8 next_index = Node::offsetToIndex(dir);
			UI16 next_node = tree.nodes.get(node_stack[max_layer])->children[next_index];

			if (isDataPtr(next_node)) {
				for (UI8 i{ current_layer }; i < kMaxLayers; i++) {
					node_stack[i] = next_node;
				}
				max_layer = kMaxLayers - 1;
				return;
			}
			
			node_stack[current_layer] = next_node;
			max_layer = current_layer;
		}
	}




	UI16 Chunk::Octree::createNode(UI16 init) {
		UI16 node = static_cast<UI16>(nodes.allocate());

		Node *n = nodes.get(node);
		for (UI8 i{ 0 }; i < 8; i++) {
			n->children[i] = init;
		}

		return node;
	}

	bool Chunk::Octree::setData(Lvec3 target, UI16 data) {
		const UI16 flaged_data = data | kFlagMask;
		if (root == flaged_data) {
			return false;
		}
		if (isDataPtr(root)) {
			root = createNode(root);
		}

		UI16 node_stack[kMaxLayers - 1]{ root };
		UI8 index_stack[kMaxLayers - 1]; // For deallocation during the collapsing

		UI8 current_layer = 1;
		while (true) {
			I8 size = kSizeForLayer[current_layer];
			Lvec3 dir{ 0, 0, 0 };

			if (target.x - size >= 0) {
				target.x -= size;
				dir.x++;
			}
			if (target.y - size >= 0) {
				target.y -= size;
				dir.y++;
			}
			if (target.z - size >= 0) {
				target.z -= size;
				dir.z++;
			}

			UI8 next_index = Node::offsetToIndex(dir);
			if (current_layer == kMaxLayers - 1) {
				nodes.get(node_stack[current_layer - 1])->children[next_index] = flaged_data;
				break;
			}

			index_stack[current_layer] = next_index;
			UI16 next_node = nodes.get(node_stack[current_layer - 1])->children[next_index];
			if (isDataPtr(next_node)) {
				if (next_node == flaged_data) {
					return false; // Nothing changed no collapsing necessary
				}

				next_node = createNode(next_node);
				nodes.get(node_stack[current_layer - 1])->children[next_index] = next_node;
			}

			node_stack[current_layer] = next_node;
			current_layer++;
		}

		bool cont{ true };
		do {
			current_layer--;

			Node *current_node = nodes.get(node_stack[current_layer]);
			UI16 data0 = current_node->children[0];
			for (UI8 i{ 1 }; (i < 8) && cont; i++) {
				if (current_node->children[i] != data0) {
					cont = false;
				}
			}

			if (cont) {
				if (current_layer == 1) {
					nodes.deallocate(root);
					root = data0;
					cont = false;
				}
				else {
					nodes.deallocate(index_stack[current_layer - 1]);
					nodes.get(node_stack[current_layer - 1])->children[index_stack[current_layer - 1]] = data0;
				}
			}
		} while (cont);

		return true;
	}

	UI16 Chunk::Octree::getData(Chunk::Lvec3 pos) const {
		TargetIterator it{ *this, pos };
		return it.max();
	}

} // namespace aquilla