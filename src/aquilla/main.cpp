#include<aquilla/world/chunk.h>
#include <aquilla/utils/memory/indexed_pool_allocator.h>

#include <iostream>
#include <chrono>
#include <list>
#include <fstream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#pragma warning(push, 0)
#include <glbinding/glbinding.h>
#include <glbinding/Binding.h>
#pragma warning(pop)

#include <glbinding-aux/Meta.h>
#include <glbinding/gl/gl.h>

namespace aquilla {
	
	const I8 kCubeVertexData[]{
		// Front face
		-1, -1, -1,	   0,  0, -1,
		-1,  1, -1,	   0,  0, -1,
		 1,  1, -1,	   0,  0, -1,
		 1, -1, -1,	   0,  0, -1,

		// Right face
		 1, -1, -1,	   1,  0,  0,
		 1,  1, -1,	   1,  0,  0,
		 1,  1,  1,	   1,  0,  0,
		 1, -1,  1,	   1,  0,  0,

		// Back face
		 1, -1,  1,	   0,  0,  1,
		 1,  1,  1,	   0,  0,  1,
		-1,  1,  1,	   0,  0,  1,
		-1, -1,  1,	   0,  0,  1,

		// Left face
		-1, -1,  1,	  -1,  0,  0,
		-1,  1,  1,	  -1,  0,  0,
		-1,  1, -1,	  -1,  0,  0,
		-1, -1, -1,	  -1,  0,  0,

		// Top face
		-1,  1, -1,	   0,  1,  0,
		-1,  1,  1,	   0,  1,  0,
		 1,  1,  1,	   0,  1,  0,
		 1,  1, -1,	   0,  1,  0,

		// Bottom face
		 1, -1, -1,	   0, -1,  0,
		 1, -1,  1,	   0, -1,  0,
		-1, -1,  1,	   0, -1,  0,
		-1, -1, -1,	   0, -1,  0,
	};

	const UI8 kCubeIndexData[]{
		 0,  1,  2,  3, 0xFF,
		 4,  5,  6,  7, 0xFF,
		 8,  9, 10, 11, 0xFF,
		12, 13, 14, 15, 0xFF,
		16, 17, 18, 19, 0xFF,
		20, 21, 22, 23,
	};

	gl::GLuint cube_vertex_vbo{ 0 };
	gl::GLuint cube_index_vbo{ 0 };
	gl::GLuint cube_vao{ 0 };

	const float kAxisVertexData[]{
		// X
		1.f, 0.f, 0.f,	 1.f, 0.f, 0.f,
		0.f, 0.f, 0.f,   1.f, 0.f, 0.f,

		// Y
		0.f, 1.f, 0.f,   0.f, 1.f, 0.f,
		0.f, 0.f, 0.f,   0.f, 1.f, 0.f,

		// Z
		0.f, 0.f, 1.f,   0.f, 0.f, 1.f,
		0.f, 0.f, 0.f,   0.f, 0.f, 1.f,
	};

	gl::GLuint axis_vertex_vbo{ 0 };
	gl::GLuint axis_vao{ 0 };

	bool loadFile(const char *path, std::vector<char> &res) {
		std::ifstream file{ path, std::ios::ate };
		if (!file) {
			std::cout << "Error loading file \"" << path << "\"" << std::endl;
			return false;
		}

		size_t length{ static_cast<size_t>(file.tellg()) };
		file.seekg(0);

		res.resize(length);
		file.read(res.data(), length);
		file.close();
		return true;
	}

	gl::GLuint createProgram(const char* vert_path, const char *frag_path) {
		gl::GLuint vert, frag;
		gl::GLsizei length;
		const char *dataptr;

		std::vector<char> data;
		if (!loadFile(vert_path, data)) {
			std::cout << "Unable to load vertex shader \"" << vert_path << "\"" << std::endl;
			return 0;
		}

		vert = gl::glCreateShader(gl::GL_VERTEX_SHADER);
		length = static_cast<gl::GLsizei>(data.size());
		dataptr = data.data();
		gl::glShaderSource(vert, 1, &dataptr, &length);

		data.clear();
		if (!loadFile(frag_path, data)) {
			std::cout << "Unable to load fragment shader \"" << vert_path << "\"" << std::endl;
			return 0;
		}

		frag = gl::glCreateShader(gl::GL_FRAGMENT_SHADER);
		length = static_cast<gl::GLsizei>(data.size());
		dataptr = data.data();
		gl::glShaderSource(frag, 1, &dataptr, &length);


		gl::glCompileShader(vert);

		gl::GLboolean res;
		gl::glGetShaderiv(vert, gl::GL_COMPILE_STATUS, &res);
		if (res != gl::GL_TRUE) {
			gl::GLint info_length;
			gl::glGetShaderiv(vert, gl::GL_INFO_LOG_LENGTH, &info_length);
			
			data.clear();
			if (info_length > 0) {
				data.resize(info_length + 1);
				data[info_length] = 0;

				gl::glGetShaderInfoLog(vert, info_length, &info_length, data.data());
			}

			std::cout << "Failed to compile vertex shader: " << data.data() << std::endl;

			gl::glDeleteShader(vert);
			gl::glDeleteShader(frag);
			return 0;
		}

		gl::glCompileShader(frag);

		gl::glGetShaderiv(frag, gl::GL_COMPILE_STATUS, &res);
		if (res != gl::GL_TRUE) {
			gl::GLint info_length;
			gl::glGetShaderiv(frag, gl::GL_INFO_LOG_LENGTH, &info_length);

			data.clear();
			if (info_length > 0) {
				data.resize(info_length + 1);
				data[info_length] = 0;

				gl::glGetShaderInfoLog(frag, info_length, &info_length, data.data());
			}

			std::cout << "Failed to compile fragment shader: " << data.data() << std::endl;

			gl::glDeleteShader(vert);
			gl::glDeleteShader(frag);
			return 0;
		}

		gl::GLuint prog{ gl::glCreateProgram() };
		gl::glAttachShader(prog, vert);
		gl::glAttachShader(prog, frag);

		gl::glLinkProgram(prog);

		gl::glDeleteShader(vert);
		gl::glDeleteShader(frag);

		gl::glGetProgramiv(prog, gl::GL_LINK_STATUS, &res);
		if (res != gl::GL_TRUE) {
			gl::GLint info_length;
			gl::glGetProgramiv(prog, gl::GL_INFO_LOG_LENGTH, &info_length);

			data.clear();
			if (info_length > 0) {
				data.resize(info_length + 1);
				data[info_length] = 0;

				gl::glGetProgramInfoLog(prog, info_length, &info_length, data.data());
			}

			std::cout << "Failed to link program: " << data.data() << std::endl;

			gl::glDeleteProgram(prog);
			return 0;
		}

		return prog;
	}

	constexpr float kMoveSpeed{ 1.f };
	glm::vec3 camera_pos{ 0.f, 0.f, -2.f };

	bool mouse_lock{ false };
	void handleKeyboardInput(GLFWwindow* window, int key, int scancode, int action, int mods) {
		if (key == GLFW_KEY_ESCAPE) {
			mouse_lock = false;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}

	constexpr float kCursorSensitivity{ 1.f };
	double mposx{ 0 }, mposy{ 0 };
	glm::vec2 camera_angle{ 0.f, 0.f };

	void handleMouseInput(GLFWwindow* window, int button, int action, int mods) {
		if (button == GLFW_MOUSE_BUTTON_1) {
			mouse_lock = true;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			glfwGetCursorPos(window, &mposx, &mposy);
		}
	}

	inline float degToRad(float angle) {
		return (angle / 360.f) * 2.f * 3.1516926f;
	}

	inline glm::mat4 rotateX(float angle) {
		angle = degToRad(angle);
		return glm::mat4{
			1.f, 0, 0, 0,
			0, cosf(angle), -sinf(angle), 0,
			0, sinf(angle), cosf(angle), 0,
			0, 0, 0, 1.f
		};
	}

	inline glm::mat4 rotateY(float angle) {
		angle = degToRad(angle);
		return glm::mat4{
			cosf(angle), 0, sinf(angle), 0,
			0, 1.f, 0, 0,
			-sinf(angle), 0, cosf(angle), 0,
			0, 0, 0, 1.f
		};
	}

	inline glm::mat4 getCameraAngleMat() {
		return rotateX(camera_angle.y) * rotateY(camera_angle.x);
	}

	inline glm::mat4 getWTOC() {
		return getCameraAngleMat() * glm::mat4{
			1.f, 0, 0, 0,
			0, 1.f, 0, 0,
			0, 0, 1.f, 0,
			-camera_pos.x, -camera_pos.y, -camera_pos.z, 1.f,
		};
	}

	using meta = glbinding::aux::Meta;
	void debugMsgCallback(gl::GLenum source, gl::GLenum type, gl::GLuint id, gl::GLenum severity, gl::GLsizei length, const gl::GLchar *msg, const void *param) {
		std::cout << "[OPENGL] (" << id << "): " << msg << std::endl;
		//std::cout << "[OPENGL] - " << meta::getString(source) << ":" << meta::getString(type) << "[" << meta::getString(severity) << "] (" << id << "): " << msg << std::endl;
	}

	void run() {
		GLFWwindow *window;

		if (!glfwInit()) {
			std::cout << "Failed to initialize GLFW" << std::endl;
			return;
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

		window = glfwCreateWindow(800, 600, "Project Aquilla", nullptr, nullptr);
		if (!window) {
			std::cout << "Failed to create window" << std::endl;
			glfwTerminate();
			return;
		}

		glfwSetKeyCallback(window, handleKeyboardInput);
		glfwSetMouseButtonCallback(window, handleMouseInput);

		glfwMakeContextCurrent(window);

		glbinding::Binding::initialize(glfwGetProcAddress);

		gl::glDebugMessageCallback((gl::GLDEBUGPROC)(debugMsgCallback), nullptr);
		gl::glDebugMessageControl(gl::GL_DONT_CARE, gl::GL_DONT_CARE, gl::GL_DONT_CARE, 0, nullptr, gl::GL_TRUE);
		gl::glDebugMessageControl(gl::GL_DONT_CARE, gl::GL_DONT_CARE, gl::GL_DEBUG_SEVERITY_HIGH, 0, nullptr, gl::GL_TRUE);
		gl::glDebugMessageControl(gl::GL_DONT_CARE, gl::GL_DONT_CARE, gl::GL_DEBUG_SEVERITY_MEDIUM, 0, nullptr, gl::GL_TRUE);


		gl::GLuint program = createProgram("../../temp.vert", "../../temp.frag");
		gl::GLuint axis_program = createProgram("../../axis.vert", "../../temp.frag");
		
		gl::glCreateBuffers(1, &cube_vertex_vbo);
		gl::glCreateBuffers(1, &cube_index_vbo);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, cube_vertex_vbo);
		gl::glBufferStorage(gl::GL_ARRAY_BUFFER, sizeof(kCubeVertexData) * sizeof(I8), kCubeVertexData, gl::GL_NONE_BIT);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, cube_index_vbo);
		gl::glBufferStorage(gl::GL_ARRAY_BUFFER, sizeof(kCubeIndexData) * sizeof(UI8), kCubeIndexData, gl::GL_NONE_BIT);
		
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);


		gl::glGenVertexArrays(1, &cube_vao);

		gl::glBindVertexArray(cube_vao);
		
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, cube_vertex_vbo);
		gl::glEnableVertexAttribArray(0);
		gl::glVertexAttribIPointer(0, 3, gl::GL_BYTE, sizeof(I8) * 6, 0);
		gl::glEnableVertexAttribArray(1);
		gl::glVertexAttribIPointer(1, 3, gl::GL_BYTE, sizeof(I8) * 6, reinterpret_cast<const void*>(sizeof(I8) * 3));
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, cube_index_vbo);

		gl::glBindVertexArray(0);


		gl::glCreateBuffers(1, &axis_vertex_vbo);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, axis_vertex_vbo);
		gl::glBufferStorage(gl::GL_ARRAY_BUFFER, sizeof(kAxisVertexData) * sizeof(float), kAxisVertexData, gl::GL_NONE_BIT);

		gl::glGenVertexArrays(1, &axis_vao);

		gl::glBindVertexArray(axis_vao);

		gl::glEnableVertexAttribArray(0);
		gl::glVertexAttribPointer(0, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 6, 0);
		gl::glEnableVertexAttribArray(1);
		gl::glVertexAttribPointer(1, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 6, reinterpret_cast<const void*>(sizeof(float) * 3));
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		gl::glBindVertexArray(0);


		gl::glEnable(gl::GL_PRIMITIVE_RESTART);
		gl::glEnable(gl::GL_DEPTH_TEST);

		gl::glFrontFace(gl::GL_CW);
		gl::glCullFace(gl::GL_BACK);
		gl::glEnable(gl::GL_CULL_FACE);
		

		const gl::GLint kMTOW{ gl::glGetUniformLocation(program, "mtow") };
		const gl::GLint kWTOS{ gl::glGetUniformLocation(program, "wtos") };
		const gl::GLint kView{ gl::glGetUniformLocation(program, "view") };

		const gl::GLint kMTOWCOL{ gl::glGetUniformLocation(axis_program, "mtow") };
		const gl::GLint kWTOSCOL{ gl::glGetUniformLocation(axis_program, "wtos") };

		glm::mat4 imat{ 1.f };
		glm::mat4 mtow{
			1.f / 16.f, 0, 0, 0,
			0, 1.f / 16.f, 0, 0,
			0, 0, 1.f / 16.f, 0,
			0, 0, 0, 1.f,
		};
		glm::mat4 cam_mat = glm::perspectiveFovLH_NO(45.f, 1.f, 0.75f, 0.1f, 100.f);



		Chunk chunk;
		/*for (int i = 0; i < 100000; i++) {
			chunk.setVoxel({
					static_cast<I8>((static_cast<double>(rand()) / static_cast<double>(RAND_MAX)) * 31),
					static_cast<I8>((static_cast<double>(rand()) / static_cast<double>(RAND_MAX)) * 31),
					static_cast<I8>((static_cast<double>(rand()) / static_cast<double>(RAND_MAX)) * 31)
				}, true);
		}*/

		chunk.setVoxel({ 1, 1, 1 }, true);
		chunk.setVoxel({ 0, 1, 1 }, true);
		chunk.setVoxel({ 2, 1, 1 }, true);
		chunk.setVoxel({ 1, 0, 1 }, true);
		chunk.setVoxel({ 1, 2, 1 }, true);
		chunk.setVoxel({ 1, 1, 0 }, true);
		chunk.setVoxel({ 1, 1, 2 }, true);


		chunk.generateMesh();
		
		if (chunk.mesh.vertex_data.size() == 0) {
			std::cout << "No Mesh" << std::endl;
		}

		gl::GLuint chunk_vertex_vbo;
		gl::GLuint chunk_index_vbo;

		gl::glGenBuffers(1, &chunk_vertex_vbo);
		gl::glGenBuffers(1, &chunk_index_vbo);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, chunk_vertex_vbo);
		gl::glBufferStorage(gl::GL_ARRAY_BUFFER, chunk.mesh.vertex_data.size(), chunk.mesh.vertex_data.data(), gl::GL_NONE_BIT);
		//gl::glBufferData(gl::GL_ARRAY_BUFFER, chunk.mesh.vertex_data.size(), chunk.mesh.vertex_data.data(), gl::GL_DYNAMIC_DRAW);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, chunk_index_vbo);
		gl::glBufferStorage(gl::GL_ARRAY_BUFFER, chunk.mesh.index_data.size() * sizeof(UI32), chunk.mesh.index_data.data(), gl::GL_NONE_BIT);
		//gl::glBufferData(gl::GL_ARRAY_BUFFER, chunk.mesh.index_data.size(), chunk.mesh.index_data.data(), gl::GL_DYNAMIC_DRAW);
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		gl::GLuint chunk_vao;
		gl::glGenVertexArrays(1, &chunk_vao);

		gl::glBindVertexArray(chunk_vao);

		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, chunk_vertex_vbo);
		gl::glEnableVertexAttribArray(0);
		gl::glVertexAttribIPointer(0, 3, gl::GL_BYTE, sizeof(I8) * 6, 0);
		gl::glEnableVertexAttribArray(1);
		gl::glVertexAttribIPointer(1, 3, gl::GL_BYTE, sizeof(I8) * 6, reinterpret_cast<const void*>(sizeof(I8) * 3));
		gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);

		gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, chunk_index_vbo);

		gl::glBindVertexArray(0);


		clock_t t0 = clock();
		while (!glfwWindowShouldClose(window)) {
			clock_t t1 = clock();
			float dt = static_cast<float>(static_cast<double>(t1 - t0) / static_cast<double>(CLOCKS_PER_SEC));
			t0 = t1;

			gl::glClear(gl::GL_COLOR_BUFFER_BIT | gl::GL_DEPTH_BUFFER_BIT);

			{
				if (mouse_lock) {
					double nposx, nposy;
					glfwGetCursorPos(window, &nposx, &nposy);
					float dx = static_cast<float>(nposx - mposx);
					float dy = static_cast<float>(nposy - mposy);
					mposx = nposx;
					mposy = nposy;

					camera_angle.x += dx * kCursorSensitivity;
					camera_angle.y += dy * kCursorSensitivity;
				
					if (camera_angle.y >= 80.f) {
						camera_angle.y = 80.f;
					}
					else if (camera_angle.y <= -80.f) {
						camera_angle.y = -80.f;
					}
				}

				glm::vec4 dir{ 0.f };
				if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
					dir.x += 1.f;
				}
				if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
					dir.x -= 1.f;
				}
				if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
					dir.y += 1.f;
				}
				if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
					dir.y -= 1.f;
				}
				if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
					dir.z += 1.f;
				}
				if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
					dir.z -= 1.f;
				}
				
				float length2 = dir.x * dir.x + dir.y * dir.y + dir.z * dir.z;
				if (length2 != 0) {
					dir = (dir / sqrtf(length2)) * kMoveSpeed * dt;

					dir = dir *getCameraAngleMat();

					camera_pos += glm::vec3{ dir };
				}
			}

			const glm::mat4 kwtos{ cam_mat * getWTOC() };

			gl::glUseProgram(program);
			gl::glBindVertexArray(chunk_vao);

			gl::glUniformMatrix4fv(kMTOW, 1, gl::GL_FALSE, &(mtow[0][0]));
			gl::glUniformMatrix4fv(kWTOS, 1, gl::GL_FALSE, &(kwtos[0][0]));
			gl::glUniform3f(kView, 0, 0, 1);

			gl::glPrimitiveRestartIndex(0xFFFF);
			//gl::glDrawArrays(gl::GL_TRIANGLE_FAN, 8, 4);
			for (int i = 0; i < 512; i++) {
				gl::glDrawElements(gl::GL_TRIANGLE_FAN, chunk.mesh.index_data.size(), gl::GL_UNSIGNED_INT, 0);
			}

			gl::glBindVertexArray(0);

			gl::glUseProgram(axis_program);
			gl::glBindVertexArray(axis_vao);

			gl::glUniformMatrix4fv(kMTOWCOL, 1, gl::GL_FALSE, &(mtow[0][0]));
			gl::glUniformMatrix4fv(kWTOSCOL, 1, gl::GL_FALSE, &(kwtos[0][0]));

			gl::glDrawArrays(gl::GL_LINES, 0, 6);

			gl::glBindVertexArray(0);
			gl::glUseProgram(0);


			glfwSwapBuffers(window);

			glfwPollEvents();
		}

		gl::glDeleteProgram(program);

		glfwTerminate();
	}

} // namespace aquilla

int main() {
	aquilla::run();

	/*aquilla::Chunk chunk;
	using v = glm::tvec3<I8>;

	chunk.setVoxel({ 0, 0, 0 }, true);
	*/
	return 0;
}